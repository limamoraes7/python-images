image: docker:git

services:
  - docker:dind

before_script:
  # Install dependencies
  - apk add --no-cache curl jq
  # Upgrade libraries
  - apk upgrade
  # Login to docker hub
  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD"
  # Get the latest successful pipeline commit
  # More info: https://gitlab.com/gitlab-org/gitlab-ce/issues/19813
  - >
    export LAST_COMMIT=$(curl --header "PRIVATE-TOKEN: $CI_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/pipelines" |
    jq -r '.[1].sha')

stages:
  - base
  - tools
  - opencv

variables:
  CI_REGISTRY_IMAGE: "${CI_REGISTRY_PATH}/${IMAGE}"
  # todo: use a variable instead of hard coded versions, however there is a variable expansion bug on gitlab-ci
  # TF_VER: "2.2.0"


# Template for all the stages stages
.docker-build:
  script:
    - ./build.sh
  artifacts:
    expire_in: 1 day
    paths:
      - variables

.tf based: &tf-based
  IMAGE: "tf"

.pt based: &pt-based
  IMAGE: "pt"

# Base (minimal) construct
.build base:
  stage: base
  extends: .docker-build

.build base vars: &build-base-vars
  IMAGE_TAG: "${VER}"
  DOCKERFILE: "base"


.build tf base vars: &build-tf-base-vars
  <<: *tf-based
  <<: *build-base-vars
  BUILD_ARGS: "--build-arg BASE_IMAGE=tensorflow/tensorflow:${VER}"
  
build tf base cpu latest:
  variables:
    VER: "latest"
    <<: *build-tf-base-vars
  extends: .build base
  
build tf base cpu:
  variables:
    VER: "2.2.0"
    <<: *build-tf-base-vars
  extends: .build base

build tf base gpu latest:
  variables:
    VER: "latest-gpu"
    <<: *build-tf-base-vars
  extends: .build base

build tf base gpu:
  variables:
    VER: "2.2.0-gpu"
    <<: *build-tf-base-vars
  extends: .build base


.build pt base vars: &build-pt-base-vars
  <<: *pt-based
  <<: *build-base-vars
  BUILD_ARGS: "--build-arg BASE_IMAGE=pytorch/pytorch:${VER}"


build pt base latest:
  variables:
    VER: "latest"
    <<: *build-pt-base-vars
  extends: .build base



# Extra tools
.build tools:
  stage: tools
  extends: .docker-build

.build tools vars: &build-tools-vars
  IMAGE_TAG: "${VER}-tools"
  DOCKERFILE: "tools"
  # I need to place the CI_REGISTRY_PATH here, since it is not expanded twice :(
  BUILD_ARGS: "--build-arg BASE_IMAGE=${CI_REGISTRY_PATH}/${IMAGE}:${VER}"


.build tf tools vas: &build-tf-tools-vars
  <<: *tf-based
  <<: *build-tools-vars  

build tf tools cpu latest:
  variables:
    VER: "latest"
    <<: *build-tf-tools-vars
  extends: .build tools
  dependencies:
    - build tf base cpu latest

build tf tools cpu:
  variables:
    VER: "2.2.0"
    <<: *build-tf-tools-vars
  extends: .build tools
  dependencies:
    - build tf base cpu

build tf tools gpu latest:
  variables:
    VER: "latest-gpu"
    <<: *build-tf-tools-vars
  extends: .build tools
  dependencies:
    - build tf base gpu latest

build tf tools gpu:
  stage: tools
  variables:
    VER: "2.2.0-gpu"
    <<: *build-tf-tools-vars
  extends: .build tools
  dependencies:
    - build tf base gpu


.build pt tools vars: &build-pt-tools-vars
  <<: *pt-based
  <<: *build-tools-vars  

build pt tools latest:
  variables:
    VER: "latest"
    <<: *build-pt-tools-vars
  extends: .build tools
  dependencies:
    - build pt base latest



# OpenCV version
.build opencv:
  stage: opencv
  extends: .docker-build

.build opencv vars: &build-opencv-vars
  IMAGE_TAG: "${VER}-opencv"
  DOCKERFILE: "opencv"
  BUILD_ARGS: "--build-arg BASE_IMAGE=${CI_REGISTRY_PATH}/${IMAGE}:${VER}"

.build tf opencv vars: &build-tf-opencv-vars
  <<: *tf-based
  <<: *build-opencv-vars

build tf opencv cpu latest:
  variables:
    VER: "latest-tools"
    <<: *build-tf-opencv-vars
  extends: .build opencv
  dependencies:
    - build tf tools cpu latest

build tf opencv cpu:
  variables:
    VER: "2.2.0-tools"
    <<: *build-tf-opencv-vars
  extends: .build opencv
  dependencies:
    - build tf tools cpu

build tf opencv gpu latest:
  variables:
    VER: "latest-gpu-tools"
    <<: *build-tf-opencv-vars
  extends: .build opencv
  dependencies:
    - build tf tools gpu latest

build tf opencv gpu:
  variables:
    VER: "2.2.0-gpu-tools"
    <<: *build-tf-opencv-vars
  extends: .build opencv
  dependencies:
    - build tf tools gpu


.build pt opencv vars: &build-pt-opencv-vars
  <<: *pt-based
  <<: *build-opencv-vars

build pt opencv latest:
  variables:
    VER: "latest-tools"
    <<: *build-pt-opencv-vars
  extends: .build opencv
  dependencies:
    - build pt tools latest
